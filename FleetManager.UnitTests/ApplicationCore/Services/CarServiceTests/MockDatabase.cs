﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using FleetManager.ApplicationCore.Services;
using FleetManager.ApplicationCore.Entities;

namespace FleetManager.UnitTests.ApplicationCore.Services.CarServiceTests
{
    public class MockDatabase
    {
        private SqliteConnection _connection;
        private DbContextOptions<CarContext> _options;

        public MockDatabase()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
        }

        public void SetupCarTable()
        {
            _options = new DbContextOptionsBuilder<CarContext>()
                .UseSqlite(_connection)
                .Options;

            // Create the car schema in the database
            using (var context = Context())
            {
                context.Database.EnsureCreated();
            }
        }

        public void AddCar(Car car)
        {
            using (var context = Context())
            {
                context.Cars.Add(car);
                context.SaveChanges();
            }
        }

        public void AddCar(IEnumerable<Car> cars)
        {
            using (var context = Context())
            {
                context.Cars.AddRange(cars);
                context.SaveChanges();
            }
        }

        public CarContext Context()
        {
            return new CarContext(_options);
        }

        public void Close()
        {
            _connection.Close();
        }
    }
}
