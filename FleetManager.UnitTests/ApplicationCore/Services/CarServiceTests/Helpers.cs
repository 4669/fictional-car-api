﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using FleetManager.ApplicationCore.Services;
using FleetManager.ApplicationCore.Entities;
using Xunit;

namespace FleetManager.UnitTests.ApplicationCore.Services.CarServiceTests
{
    public class Helpers
    {
        
        public static void AssertEqualCar(Car c1, Car c2)
        {
            Assert.Equal(c1.Make, c2.Make);
            Assert.Equal(c1.Model, c2.Model);
            Assert.Equal(c1.Registration, c2.Registration);
            Assert.Equal(c1.Year, c2.Year);
            Assert.Equal(c1.InspectionDate, c2.InspectionDate);
            Assert.Equal(c1.EngineSize, c2.EngineSize);
            Assert.Equal(c1.EnginePower, c2.EnginePower);
        }

        public static Car ValidSampleCar() {
            return new Car
            {
                Id = Guid.Parse("d9417f10-5c79-44a0-9137-4eba914a82a9"),
                Make = "Tojota",
                Model = "Hihase",
                Registration = "BEN-150",
                Year = 1998,
                InspectionDate = "1999-12-24",
                EngineSize = 100,
                EnginePower = 210
            };
        }

        public static List<Car> ValidSampleCars() {
            return new List<Car>()
            {
                ValidSampleCar(),
                new Car {
                    Id = Guid.Parse("d9417f10-5c79-44a0-9137-4eba914a82a8"),
                    Make = "Tojota",
                    Model = "Jarise",
                    Registration = "BEN-213",
                    Year = 1999,
                    InspectionDate = "2001-12-24",
                    EngineSize = 101,
                    EnginePower = 211
                },
                new Car {
                    Id = Guid.Parse("d9417f10-5c79-44a0-9137-4eba914a82a7"),
                    Make = "Mersu",
                    Model = "Marsu",
                    Registration = "MER-666",
                    Year = 2010,
                    InspectionDate = "2012-01-17",
                    EngineSize = 200,
                    EnginePower = 543
                },
                new Car {
                    Id = Guid.Parse("d9417f10-5c79-44a0-9137-4eba914a82a6"),
                    Make = "Tojota",
                    Model = "Hihase",
                    Registration = "HEE-345",
                    Year = 1969,
                    InspectionDate = "2015-02-21",
                    EngineSize = 112,
                    EnginePower = 69
                },
                new Car {
                    Id = Guid.Parse("d9417f10-5c79-44a0-9137-4eba914a82a5"),
                    Make = "Mersu",
                    Model = "Mursu",
                    Registration = "HOI-825",
                    Year = 2006,
                    InspectionDate = "2007-11-15",
                    EngineSize = 98,
                    EnginePower = 55
                },
                new Car {
                    Id = Guid.Parse("d9417f10-5c79-44a0-9137-4eba914a82a4"),
                    Make = "",
                    Model = "",
                    Registration = "",
                    Year = 0,
                    InspectionDate = "",
                    EngineSize = 0,
                    EnginePower = 0
                }
            };
        }
    }
}
