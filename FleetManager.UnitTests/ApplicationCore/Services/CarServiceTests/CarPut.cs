﻿
using System;
using System.Linq;
using FleetManager.ApplicationCore.Interfaces;
using FleetManager.ApplicationCore.Services;
using FleetManager.ApplicationCore.Entities;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;

namespace FleetManager.UnitTests.ApplicationCore.Services.CarServiceTests
{
    public class CarPut
    {

        [Fact]
        public async void PutExistingCar()
        {
            var mockDatabase = new MockDatabase();
            Car validSampleCar = Helpers.ValidSampleCar();
            Car editedCar = Helpers.ValidSampleCar();
            editedCar.InspectionDate = "2002-02-02";

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCar);

                // Use Put to edit the car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Put(editedCar);
                }

                // Use a separate instance of the context to verify that editing took place
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(carFromDatabase, editedCar);
                }
            }
            finally
            {
                mockDatabase.Close();
            }

        }

        [Fact]
        public async void PutNonExistingCar()
        {
            var mockDatabase = new MockDatabase();
            Car car = Helpers.ValidSampleCar();
            Car carWithWrongId = Helpers.ValidSampleCar();
            carWithWrongId.Id = Guid.NewGuid();
            carWithWrongId.Make = "FolksWagon";

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(car);

                // Use Put to edit the wrong car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    bool editSuccessful = await service.Put(carWithWrongId);
                    
                    Assert.False(editSuccessful);
                }

                // Check that sample car has not changed
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(carFromDatabase, car);
                }
            }
            catch (DbUpdateConcurrencyException)
            {
            }
            finally
            {
                mockDatabase.Close();
            }

        }

        [Fact]
        public async void PutWithEmptyDatabase()
        {
            var mockDatabase = new MockDatabase();
            Car carToPut = Helpers.ValidSampleCar();

            try
            {
                mockDatabase.SetupCarTable();

                // Try to use Put
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    bool editSuccessful = await service.Put(carToPut);
                    
                    Assert.False(editSuccessful);
                }

                // Check that Put did not add any cars
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());
                }
            }
            catch (DbUpdateConcurrencyException)
            {
            }
            finally
            {
                mockDatabase.Close();
            }

        }

        [Theory]
        [InlineData(null, null, null, null)]
        [InlineData(null, "", "", "")]
        [InlineData("", null, "", "")]
        [InlineData("", "", null, "")]
        [InlineData("", "", "", null)]
        public async void PutCarWithNulls(string make, string model, string registration, string inspectiondate)
        {
            var mockDataabse = new MockDatabase();

            Car validSampleCar = Helpers.ValidSampleCar();
            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Make = make;
            invalidCar.Model = model;
            invalidCar.Registration = registration;
            invalidCar.InspectionDate = inspectiondate;

            try
            {
                mockDataabse.SetupCarTable();
                mockDataabse.AddCar(validSampleCar);

                // Put the invalid car
                using (var context = mockDataabse.Context())
                {
                    var service = new CarService(context);
                    bool editSuccessful = await service.Put(invalidCar);

                    Assert.False(editSuccessful);
                }

                // Check that sample car has not changed
                using (var context = mockDataabse.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(carFromDatabase, validSampleCar);
                }
            }
            finally
            {
                mockDataabse.Close();
            }
        }

        [Fact]
        public async void PutCarWithTooLongMake()
        {
            var mockDatabase = new MockDatabase();

            string TooLongString = new string('a', 2001);

            Car validSampleCar = Helpers.ValidSampleCar();
            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Make = TooLongString;

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCar);
                
                // Edit the valid car to an invalid car using Put
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Put(invalidCar);
                }

                // Check that sample car has not changed
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(carFromDatabase, validSampleCar);
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void PutCarWithTooLongModel()
        {
            var mockDatabase = new MockDatabase();

            string TooLongString = new string('a', 5001);

            Car validSampleCar = Helpers.ValidSampleCar();
            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Model = TooLongString;

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCar);

                // Edit the valid car to an invalid car using Put
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Put(invalidCar);
                }

                // Check that sample car has not changed
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(carFromDatabase, validSampleCar);
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void PutCarWithTooLongRegistration()
        {
            var mockDatabase = new MockDatabase();

            string TooLongString = new string('a', 51);

            Car validSampleCar = Helpers.ValidSampleCar();
            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Registration = TooLongString;

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCar);

                // Edit the valid car to an invalid car using Put
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Put(invalidCar);
                }

                // Check that sample car has not changed
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(carFromDatabase, validSampleCar);
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void PutCarWithTooLongInspectionDate()
        {
            var mockDatabase = new MockDatabase();

            string TooLongString = new string('a', 16);

            Car validSampleCar = Helpers.ValidSampleCar();
            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.InspectionDate = TooLongString;

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCar);

                // Edit the valid car to an invalid car using Put
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Put(invalidCar);
                }

                // Check that sample car has not changed
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(validSampleCar, carFromDatabase);
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

    }

}

