﻿
using System;
using System.Linq;
using FleetManager.ApplicationCore.Interfaces;
using FleetManager.ApplicationCore.Services;
using FleetManager.ApplicationCore.Entities;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;

namespace FleetManager.UnitTests.ApplicationCore.Services.CarServiceTests
{
    public class CarPost
    {

        [Fact]
        public async void PostValidCar()
        {
            var mockDatabase = new MockDatabase();
            var validSampleCar = Helpers.ValidSampleCar();

            try
            {
                mockDatabase.SetupCarTable();

                // Try to Post a car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Post(validSampleCar);
                }

                // Verify 
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());

                    Car carFromDatabase = context.Cars.Single();
                    Helpers.AssertEqualCar(carFromDatabase, validSampleCar);
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Theory]
        [InlineData(null, null, null, null)]
        [InlineData(null, "", "", "")]
        [InlineData("", null, "", "")]
        [InlineData("", "", null, "")]
        [InlineData("", "", "", null)]
        public async void PostCarWithNulls(string make, string model, string registration, string inspectiondate)
        {
            var mockDatabase = new MockDatabase();

            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Make = make;
            invalidCar.Model = model;
            invalidCar.Registration = registration;
            invalidCar.InspectionDate = inspectiondate;

            try
            {
                mockDatabase.SetupCarTable();

                // Post the invalid car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Post(invalidCar);
                }

                // Check that no car was added
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());

                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void PostCarWithTooLongMake()
        {
            var mockDatabase = new MockDatabase();

            string TooLongMake = new string('a', 2001);

            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Make = TooLongMake;

            try
            {
                mockDatabase.SetupCarTable();

                // Post the invalid car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Post(invalidCar);
                }

                // Check that no car was added
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void PostCarWithTooLongModel()
        {
            var mockDatabase = new MockDatabase();
            string TooLongModel = new string('a', 5001);

            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Model = TooLongModel;

            try
            {
                mockDatabase.SetupCarTable();

                // Post the invalid car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Post(invalidCar);
                }

                // Check that no car was added
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void PostCarWithTooLongRegistration()
        {
            var mockDatabase = new MockDatabase();
            string TooLongRegistration = new string('a', 51);

            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.Registration = TooLongRegistration;
            
            try
            {
                mockDatabase.SetupCarTable();

                // Post the invalid car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Post(invalidCar);
                }

                // Check that no car was added
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void PostCarWithTooLongInspectionDate()
        {
            var mockDatabase = new MockDatabase();
            string TooLongInspectionDate = new string('a', 51);

            Car invalidCar = Helpers.ValidSampleCar();
            invalidCar.InspectionDate = TooLongInspectionDate;

            try
            {
                mockDatabase.SetupCarTable();

                // Post the invalid car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Post(invalidCar);
                }

                // Check that no car was added
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }
    }
}

