﻿
using System;
using System.Linq;
using FleetManager.ApplicationCore.Interfaces;
using FleetManager.ApplicationCore.Services;
using FleetManager.ApplicationCore.Entities;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;

namespace FleetManager.UnitTests.ApplicationCore.Services.CarServiceTests
{

    public class CarGet
    {
        
        [Fact]
        public async void AllCars()
        {
            var mockDatabase = new MockDatabase();
            var validSampleCars = Helpers.ValidSampleCars();

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCars);

                // Test & verify CarService.GetAll()
                using (var context = mockDatabase.Context())
                {
                    CarService carService = new CarService(context);
                    var cars = (await carService.GetAll()).ToList();

                    Assert.NotNull(cars);
                    Assert.NotEmpty(cars);
                    Assert.Equal(6, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }
            
        }

        [Theory]
        [InlineData(null, null, int.MinValue, int.MaxValue, 6)]                 // filter nothing, get 6 cars
        [InlineData("Tojota", null, int.MinValue, int.MaxValue, 3)]             // filter make, get 3 cars
        [InlineData("Mersu", null, int.MinValue, int.MaxValue, 2)]              // filter make, get 2 cars
        [InlineData(null, "Hihase", int.MinValue, int.MaxValue, 2)]             // filter model, get 2 cars
        [InlineData(null, null, 2000, int.MaxValue, 2)]                         // filter minvalue, get 2 cars
        [InlineData(null, null, 1999, int.MaxValue, 3)]                         // filter minvalue, get 3 cars (inclusive limit)
        [InlineData(null, null, int.MinValue, 2000, 4)]                         // filter maxvalue, get 4 cars
        [InlineData("Tojota", "Hihase", int.MinValue, int.MaxValue, 2)]         // filter make and model, get 2 cars
        [InlineData("", "", 0, 0, 1)]                                           // filter all variables, get 1 cars
        [InlineData("", "", 666, 2100, 0)]                                      // filter all variables, get 0 cars
        [InlineData(null, null, 2100, 0, 0)]                                    // minvalue > maxvalue, get 0 cars
        [InlineData("\n", null, -69, 6969, 0)]                                  // make is a strange string, get 0 cars
        [InlineData(null, "💩", 2100, 0, 0)]                                    // model is a pile of poop, get 0 cars
        [InlineData("SELECT * FROM Cars; DROP TABLE Cars", null, -69, 6969, 0)] // model is a bad string, get 0 cars
        public async void FilteredGetAll(string Make, string Model, int MinYear, int MaxYear, int ExpectedCarCount)
        {
            var mockDatabase = new MockDatabase();

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(Helpers.ValidSampleCars());

                using (var context = mockDatabase.Context())
                {
                    CarService carService = new CarService(context);
                    var cars = (await carService.GetAll(Make, Model, MinYear, MaxYear));

                    Assert.NotNull(cars);
                    Assert.Equal(ExpectedCarCount, cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void ExistingCardWithId()
        {
            var mockDatabase = new MockDatabase();

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(Helpers.ValidSampleCars());

                // Use a clean instance of the context to run the test
                using (var context = mockDatabase.Context())
                {
                    CarService carService = new CarService(context);
                    var car = (await carService.Get(Helpers.ValidSampleCar().Id));

                    Assert.NotNull(car);
                    Helpers.AssertEqualCar(Helpers.ValidSampleCar(), car);
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }

        [Fact]
        public async void NonExistingCarWithId()
        {
            var mockDatabase = new MockDatabase();

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(Helpers.ValidSampleCars());

                using (var context = mockDatabase.Context())
                {
                    CarService carService = new CarService(context);
                    var testId = Guid.Parse("69696969-6969-6969-6969-696969696969");
                    var car = (await carService.Get(testId));

                    Assert.Null(car);
                }
            }
            finally
            {
                mockDatabase.Close();
            }
        }
        
    }

}

