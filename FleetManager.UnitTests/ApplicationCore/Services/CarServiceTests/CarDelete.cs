﻿
using System;
using System.Linq;
using FleetManager.ApplicationCore.Interfaces;
using FleetManager.ApplicationCore.Services;
using FleetManager.ApplicationCore.Entities;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;

namespace FleetManager.UnitTests.ApplicationCore.Services.CarServiceTests
{
    public class CarDelete
    {

        [Fact]
        public async void DeleteExistingCar()
        {
            var mockDatabase = new MockDatabase();
            Car validSampleCar = Helpers.ValidSampleCar();

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCar);

                // Use Delete to remove the car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    await service.Delete(validSampleCar.Id);
                }

                // Use a separate instance of the context to verify that the car is gone
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }

        }

        [Fact]
        public async void DeleteCarEmptyDatabase()
        {
            var mockDatabase = new MockDatabase();
            
            try
            {
                mockDatabase.SetupCarTable();

                // Attempt to Delete a car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    Car deletedCar = await service.Delete(Guid.NewGuid());
                    
                    // Check that CarService returns null if delete fails
                    Assert.Null(deletedCar);
                }

                // Check that there are no cars in the database
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(0, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }

        }

        [Fact]
        public async void DeleteCarWithWrongId()
        {

            var mockDatabase = new MockDatabase();
            Car validSampleCar = Helpers.ValidSampleCar();

            try
            {
                mockDatabase.SetupCarTable();
                mockDatabase.AddCar(validSampleCar);

                // Use Delete to remove the wrong car
                using (var context = mockDatabase.Context())
                {
                    var service = new CarService(context);
                    Car deletedCar = await service.Delete(Guid.NewGuid());
                    
                    // Check that CarService returns a null car
                    Assert.Null(deletedCar);
                }

                // Verify that the car is still there
                using (var context = mockDatabase.Context())
                {
                    Assert.Equal(1, context.Cars.Count());
                }
            }
            finally
            {
                mockDatabase.Close();
            }

        }


    }

}

