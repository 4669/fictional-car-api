﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using FleetManager.ApplicationCore.Entities;
using FleetManager.ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FleetManager.ApplicationCore.Services
{
    public class CarService : ICarService
    {

        private readonly CarContext _context;

        public CarService(CarContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<Car>> GetAll(string make=null, string model=null, int minYear=int.MinValue, int maxYear=int.MaxValue)
        {
            return await _context.Cars
                .Where<Car>(c => 
                (make  == null || c.Make  == make)  &&
                (model == null || c.Model == model) &&
                (c.Year >= minYear)                 &&
                (c.Year <= maxYear)
                ).ToListAsync();
        }

        public async Task<Car> Get(Guid id)
        {
            return await _context.Cars.FindAsync(id);
        }
        public async Task<int> Post(Car car)
        {
            if (car.IsValid())
            {
                _context.Cars.Add(car);
            }
            return await _context.SaveChangesAsync();
        }

        public async Task<bool> Put(Car car)
        {
            if (!car.IsValid())
            {
                return false;
            }

            _context.Entry(car).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException)
            {
                if (CarExists(car.Id)) {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private bool CarExists(Guid id)
        {
            return _context.Cars.Any(car => car.Id == id);
        }

        public async Task<Car> Delete(Guid id)
        {
            Car carToDelete = await _context.Cars.FindAsync(id);
            if (carToDelete != null)
            {
                _context.Cars.Remove(carToDelete);
                await _context.SaveChangesAsync();
            }
            return carToDelete;
        }


    }
}