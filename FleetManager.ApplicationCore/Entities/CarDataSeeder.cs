﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;

namespace FleetManager.ApplicationCore.Entities
{
    public class CarDataSeeder
    {

        public static void Seed(CarContext context, string pathToCsv)
        {

            if (!context.Cars.Any())
            {
                try
                {
                    PopulateWithCarsFromCsv(context, pathToCsv);
                }
                catch
                {
                    // Ignore all thrown exceptions
                }
            }
        }

        private static void PopulateWithCarsFromCsv(CarContext context, string pathToCsv)
        {
            
            using (var reader = new StreamReader(pathToCsv))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.RegisterClassMap<CarCsvRowMap>();
                csv.Configuration.HasHeaderRecord = true;
                csv.Configuration.Delimiter = ",";

                var rows = csv.GetRecords<CarCsvRow>();
                foreach (CarCsvRow row in rows)
                {
                    Car CarToAdd = new Car
                    {
                        Id = Guid.NewGuid(),
                        Make = row.Make,
                        Model = row.Model,
                        Registration = row.Plate,
                        Year = row.Year,
                        InspectionDate = row.Date,
                        EngineSize = row.EngineSize,
                        EnginePower = row.EnginePower
                    };
                    context.Add<Car>(CarToAdd);
                }
            }
            context.SaveChanges();
        }

        public class CarCsvRow
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public string Plate { get; set; }
            public int Year { get; set; }
            public string Date { get; set; }
            public int EngineSize { get; set; }
            public int EnginePower { get; set; }
        }
        
        public class CarCsvRowMap : ClassMap<CarCsvRow>
        {
            public CarCsvRowMap()
            {
                Map(m => m.Make).Name("make");
                Map(m => m.Model).Name("model");
                Map(m => m.Plate).Name("registration");
                Map(m => m.Year).Name("year");
                Map(m => m.Date).Name("inspection date");
                Map(m => m.EngineSize).Name("engine size");
                Map(m => m.EnginePower).Name("engine power");
            }
        }
    }
}
