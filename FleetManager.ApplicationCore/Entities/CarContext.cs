﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;

namespace FleetManager.ApplicationCore.Entities
{
    public class CarContext : DbContext
    {

        public CarContext(DbContextOptions<CarContext> options)
            : base(options)
        { }

        public DbSet<Car> Cars { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
            {
                return;
            }
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../FleetManager.Web/appsettings.json");
            var configuration = builder.Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DatabaseConnection"));
        }

    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CarContext>
    {
        public CarContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../FleetManager.Web/appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<CarContext>();
            var connectionString = configuration.GetConnectionString("DatabaseConnection");
            builder.UseSqlServer(connectionString);
            return new CarContext(builder.Options);
        }
    }

}
