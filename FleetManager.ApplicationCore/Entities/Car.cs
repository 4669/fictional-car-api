﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace FleetManager.ApplicationCore.Entities
{
    public class Car
    {
        public Guid Id { get; set; } 
        [Required]
        [StringLength(2000, ErrorMessage = "The Make value cannot exceed 2000 characters. ")]
        public string Make { get; set; }
        [Required]
        [StringLength(5000, ErrorMessage = "The Model value cannot exceed 5000 characters. ")]
        public string Model { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "The Registration value cannot exceed 50 characters. ")]
        public string Registration { get; set; }
        public int Year { get; set; }
        [Required]
        [StringLength(15, ErrorMessage = "The InspectionDate value cannot exceed 15 characters. ")]
        public string InspectionDate { get; set; }
        public int EngineSize { get; set; }
        public int EnginePower { get; set; }

        // Checks if the car is valid. Valid means that it can be safely inserted to the database.
        public bool IsValid()
        {
            if (Make == null || Model == null || Registration == null || InspectionDate == null)
            {
                return false;
            }
            if (Make.Length > 2000 || Model.Length > 5000 || Registration.Length > 50 || InspectionDate.Length > 15)
            {
                return false;
            }
            return true;
        }

    }

}