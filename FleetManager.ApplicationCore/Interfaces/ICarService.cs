﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FleetManager.ApplicationCore.Entities;

namespace FleetManager.ApplicationCore.Interfaces
{
    public interface ICarService
    {
        Task<IEnumerable<Car>> GetAll(string make, string model, int minYear, int maxYear);

        Task<Car> Get(Guid id);

        Task<int> Post(Car car);

        Task<bool> Put(Car car);

        Task<Car> Delete(Guid id);

    }
}