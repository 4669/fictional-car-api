# fleetmanager

A fake software for a fake customer. Provides an API for managing a "fleet" of cars.

### Story

A fictional customer wants an API for managing their fleet of cars. Someone has started to develop that API. They did not finish it. I, the fictional API back-end expert, am to finish what was started. I am to honor their previous work and am not allowed to start from scratch nor change the development language. I am to deliver a working product with some unit test and documentation with an emphasis on future expandability.

### Description

The half-done solution consisted of three projects: ApplicationCore, Web and UnitTests. The original developer had a separation of concerns in their mind. They split the Web service and the business logic to different projects. The two projects are to communicate with each other using an interface called ICarService. The unit tests were targeted towards said interface.

### Goal

The fake customer wants to be able to add, remove, edit, list and list-with-filter the cars in their fleet. This is to be done via the Internet and with zero authentication. What a great idea.

The fake customer has a solid idea of what information a car can have. They want a car to be able to have a make, a model, a registration (aka. license plate), a made-in-year, an inspection date, an engine size and an engine power.

### Solution

Although the fake customer never explicitly said so, it seems that they want a somewhat RESTful API. They want a relational database of some kind to function as a backbone.

I developed a basic API using EntityFrameworkCore. I provided the API using HTTP requests: GET, POST, PUT and DELETE. A Microsoft SQL Server became the database.



## Using fleetmanager

### Building & Running

This repository provides you, the user, with the full source code of fleetmanager. To use this software, you must build and run it. Personally I use Visual Studio 2019 to open the solution. Then I build and run it with CTRL+F5. Feel free to build and run the solution however you wish.

### Configuring

The fleetmanager software needs a tiny bit of configuring. If you, the user, have a particular database you want to use, please change the value of "ConnectionStrings.DatabaseConnection" in appsettings.json. That's all there is to configure.

### Using

Whenever fleetmanager starts, it looks for a table called Cars. If said table exists but is empty, it will be populated with seed data from the file in \seed_data\car.csv. If you don't want that seed data in your database, delete, move or rename car.csv.

After fleetmanager is up and running, a nice and slick documentation of the API can be viewed at {HOST}:{PORT}/swagger/. Here {HOST} stands for your host, e.g. badgerbadgerbadger.com, and PORT stands for your port, e.g. 1337.

### Testing

##### with OpenAPI/swagger

To test the public facing API, open {HOST}:{PORT}/swagger/ in your favorite browser after running fleetmanager. A user interface with instructions and colorful buttons for testing should appear. Open one of the supported requests from the swagger menu and send it to the API to test how fleetmanager reacts.

[Link for more information on swagger](https://swagger.io/)

##### with Postman

Here are examples of all four supported HTTP requests:

Get all car information:

```
HTTP GET: api/car
```

Get a list of cars with filtering parameters:

```
HTTP GET: api/car?make=Tojota&model=Hihase&minyear=1999&maxyear=2007
```

You will receive a JSON list in the response body. The list will have the cars matching your query as JSON objects. The example query will find all Tojota Hihase cars made between January first of 1999 and December 31st of 2007.

Get the information of a particular car based on an id:

```
HTTP GET: api/car/570890e2-8007-4e5c-a8d6-c3f670d8a9be
```

Your response should contain the car in question as JSON in the response body. If no car is found, the response will have the 404 header.



Add a new car to the fleet:

```
HTTP POST: api/car
```

Your request body should have e.g.:

```javascript
{
	"make":"PMV",
	"model":"Model69",
	"registration":"ABC-123",
	"year":2001,
	"inspectionDate":"2018-04-15",
	"engineSize":1686,
	"enginePower":97
}
```

If you don't have a valid car in the request body, you will receive a 400 header. Otherwise, You will receive a created header and the newly added car in the response body.

> Please note that only valid cars are added to the fleet. A valid car has its make, model, registration and isnpectiondate as non-null strings. A valid car JSON object has the following maximum string lengths: make 2000, model 5000, registration 50, isnpectiondate 15.
>
> These limitations exist for the convenience of the other users of fleetmanager.



Change a cars information:

```
HTTP PUT: api/car/570890e2-8007-4e5c-a8d6-c3f670d8a9be
```

Your request body should have e.g.:

```javascript
{
	"id": "570890e2-8007-4e5c-a8d6-c3f670d8a9be",
	"make":"PMV",
	"model":"Model69",
	"registration":"ABC-123",
	"year":2001,
	"inspectionDate":"2019-04-18",
	"engineSize":1686,
	"enginePower":123
}
```

Returns a no content header when the edit was successful. 

Returns a 400 header when the edit would make the car invalid.

Returns a 404 header when the provided id does not match a car.

> Please note that only valid cars are PUT to the fleet. A valid car has its make, model, registration and isnpectiondate as non-null strings. A valid car JSON object has the following maximum string lengths: make 2000, model 5000, registration 50, isnpectiondate 15.
>
> These limitations exist for the convenience of the other users of fleetmanager.





Remove a car from the fleet:

```
HTTP DELETE: api/car/570890e2-8007-4e5c-a8d6-c3f670d8a9be
```

Returns the removed car with an OK hearer if the removal was successful. Returns a 404 header if your id did not match a car.



[Link for more information on Postman](https://www.getpostman.com/products)

##### with curl

Put something like this in your bash terminal:

~~~bash
curl -X GET "http://{HOST}:{PORT}/api/car/3fa85f64-5717-4562-b3fc-2c963f66afa6" -H  "accept: */*"
~~~

For more information on curl, run

```bash
man curl
```

### Running unit tests

Unit tests for ICarService are available. You can run them with dotnet CLI or Visual Studio. 

[How to run unit tests with dotnet CLI](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-test?tabs=netcore21)

[How to run unit tests with Visual Studio](https://docs.microsoft.com/en-us/visualstudio/test/run-unit-tests-with-test-explorer)

