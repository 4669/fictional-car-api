﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using FleetManager.ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace FleetManager.Web.Extensions
{
    public static class Extensions
    {
        public static IWebHost SeedDatabase(this IWebHost webHost)
        {
            // Hard coded path! 
            string seedDataPath = (@Directory.GetCurrentDirectory() + "/../seed_data/car.csv");
            
            var serviceScopeFactory = (IServiceScopeFactory)webHost.Services.GetService(typeof(IServiceScopeFactory));
            using (var scope = serviceScopeFactory.CreateScope())
            {
                var services = scope.ServiceProvider;
                var dbContext = services.GetRequiredService<CarContext>();

                CarDataSeeder.Seed(dbContext, seedDataPath);
            }

            return webHost;
        }
    }
}
