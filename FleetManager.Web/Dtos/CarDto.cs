﻿using System;

namespace FleetManager.Web.Models
{
    public class CarDto
    {
        public Guid Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Registration { get; set; }
        public int Year { get; set; }
        public string InspectionDate { get; set; }
        public int EngineSize { get; set; }
        public int EnginePower { get; set; }
    }

}