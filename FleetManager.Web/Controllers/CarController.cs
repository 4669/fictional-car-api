﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FleetManager.ApplicationCore.Entities;
using FleetManager.ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using FleetManager.Web.Models;
using Microsoft.AspNetCore.WebUtilities;

namespace FleetManager.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CarController : Controller
    {
        private readonly ICarService _carService;

        public CarController(ICarService carService)
        {
            _carService = carService;
        }

        /// <summary>
        /// Fetches all cars. Can be filtered with the correct querystring parameters.
        /// </summary>
        /// <remarks>
        /// To get all cars:
        /// 
        ///     HTTP GET: api/car
        /// 
        /// To get all cars matching a search:
        /// 
        ///     HTTP GET: api/car?make=Tojota
        ///     
        /// You can add more filtering parameters with ampersign.
        /// </remarks>
        /// <param name="make">Limits results to only cars with the specified make.</param> 
        /// <param name="model">Limits results to only cars with the specified model.</param>
        /// <param name="minyear">Limits results to only cars made in the specified year or later.</param>
        /// <param name="maxyear">Limits results to only cars made in the specified year or earlier.</param>
        /// <returns>Returns all (matching) cars</returns>
        /// <response code="200">Returns all (matching) cars or an empty list</response>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IEnumerable<CarDto>> Get([FromQuery] string make=null, string model=null, int minyear=int.MinValue, int maxyear=int.MaxValue)
        {
            return (await _carService
                .GetAll(make, model, minyear, maxyear))
                .Select(c => new CarDto
            {
                Id = c.Id,
                Make = c.Make,
                Model = c.Model,
                Registration = c.Registration,
                Year = c.Year,
                InspectionDate = c.InspectionDate,
                EngineSize = c.EngineSize,
                EnginePower = c.EnginePower
            });
        }

        /// <summary>
        /// Fetches a car by id.
        /// </summary>
        /// <remarks>
        /// Example:
        /// 
        ///     HTTP GET: api/car/570890e2-8007-4e5c-a8d6-c3f670d8a9be
        ///     
        /// </remarks>
        /// <param name="id">The id of the requested car</param>
        /// <returns>Returns the information of the specified car</returns>
        /// <response code="200">Returns the requested car</response>
        /// <response code="404">The requested car was not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(Guid id)
        {
            var car = await _carService.Get(id);
            if (car == null)
            {
                return NotFound();
            }

            return Ok(new CarDto
            {
                Id = car.Id,
                Make = car.Make,
                Model = car.Model,
                Registration = car.Registration,
                Year = car.Year,
                InspectionDate = car.InspectionDate,
                EngineSize = car.EngineSize,
                EnginePower = car.EnginePower
            });
        }

        /// <summary>
        /// Adds the submitted car to the fleet.
        /// </summary>
        /// <remarks>
        /// Example request:
        /// 
        ///     HTTP POST: api/car
        ///     {
        ///         "make":"PMV",
        ///         "model":"Model69",
        ///         "registration":"ABC-123",
        ///         "year":2001,
        ///         "inspectionDate":"2018-04-15",
        ///         "engineSize":1686,
        ///         "enginePower":97
        ///     }
        ///     
        /// Please note that only valid cars are added to the fleet.
        /// A valid car has its make, model, registration and isnpectiondate as non-null strings.
        /// A valid car JSON object has the following maximum string lengths: make 2000, model 5000, registration 50, isnpectiondate 15.
        /// These limitations exist for the convenience of the other users of fleetmanager.
        /// 
        /// </remarks>
        /// <param name="carDto">The car to be added</param> 
        /// <returns>Returns the appropriate status code</returns>
        /// <response code="201">Returns the newly added car</response>
        /// <response code="400">Returns when no valid car is provided in the request body</response>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]CarDto carDto)
        {
            Car carToAdd = new Car
            {
                Id = Guid.NewGuid(),
                Make = carDto.Make,
                Model = carDto.Model,
                Registration = carDto.Registration,
                Year = carDto.Year,
                InspectionDate = carDto.InspectionDate,
                EngineSize = carDto.EngineSize,
                EnginePower = carDto.EnginePower
            };
            carDto.Id = carToAdd.Id;

            if (!carToAdd.IsValid())
            {
                return BadRequest();
            }

            await _carService.Post(carToAdd);
            return CreatedAtAction("Get", new { id = carDto.Id }, carDto);

        }

        /// <summary>
        /// Edit an existing car.
        /// </summary>
        /// <remarks>
        /// Example request:
        /// 
        ///     HTTP HTTP PUT: api/car/570890e2-8007-4e5c-a8d6-c3f670d8a9be
        ///     {
        ///         "id": "570890e2-8007-4e5c-a8d6-c3f670d8a9be",
        ///         "make":"PMV",
        ///         "model":"Model69",
        ///         "registration":"ABC-123",
        ///         "year":2001,
        ///         "inspectionDate":"2019-04-18",
        ///         "engineSize":1686,
        ///         "enginePower":123
        ///     }
        ///     
        /// Please note that only valid cars are edited.
        /// A valid car has its make, model, registration and isnpectiondate as non-null strings.
        /// A valid car JSON object has the following maximum string lengths: make 2000, model 5000, registration 50, isnpectiondate 15.
        /// These limitations exist for the convenience of the other users of fleetmanager.
        /// 
        /// </remarks>
        /// <param name="id">The id of the car to be edited</param> 
        /// <param name="carDto">The car to be edited (including the id)</param> 
        /// <returns>Returns the appropriate status code, no body</returns>
        /// <response code="204">Returns no content when the edit was succesful</response>
        /// <response code="400">Returns when the your car is not valid</response>
        /// <response code="404">Returns when provided the id does not match a car</response>
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Put(Guid id, [FromBody]CarDto carDto)
        {
            Car carToPut = new Car
            {
                Id = carDto.Id,
                Make = carDto.Make,
                Model = carDto.Model,
                Registration = carDto.Registration,
                Year = carDto.Year,
                InspectionDate = carDto.InspectionDate,
                EngineSize = carDto.EngineSize,
                EnginePower = carDto.EnginePower
            };

            if (!carToPut.IsValid() || id != carDto.Id)
            {
                return BadRequest();
            }
            
            if (await _carService.Put(carToPut)) {
                return NoContent();
            }

            return NotFound();
        }

        /// <summary>
        /// Removes a car from the fleet.
        /// </summary>
        /// <remarks>
        /// Example request:
        /// 
        ///     HTTP HTTP DELETE: api/car/570890e2-8007-4e5c-a8d6-c3f670d8a9be
        /// 
        /// </remarks>
        /// <param name="id">The id of the car to be removed</param> 
        /// <returns>Returns the information of the removed car</returns>
        /// <response code="200">Returns when the deletion was succesfull</response>
        /// <response code="404">Returns when provided id does not match a car</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(Guid id)
        {
            Car deletedCar = await _carService.Delete(id);

            if (deletedCar == null)
            {
                return NotFound();
            }

            return Ok(new CarDto
            {
                Id = deletedCar.Id,
                Make = deletedCar.Make,
                Model = deletedCar.Model,
                Registration = deletedCar.Registration,
                Year = deletedCar.Year,
                InspectionDate = deletedCar.InspectionDate,
                EngineSize = deletedCar.EngineSize,
                EnginePower = deletedCar.EnginePower
            });

        }

    }

}