﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using FleetManager.Web.Extensions;

namespace FleetManager.Web
{
    public class Program
    {
        
        public static void Main(string[] args)
        {
            BuildWebHost(args)
                .SeedDatabase()
                .Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

        
    }

    
}